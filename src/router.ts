import SearchComponent from './components/Search.vue';
import SubscriptionsComponent from './components/Subscriptions.vue';
import VueRouter from 'vue-router';
import MovieReleasesComponent from './components/MovieReleases.vue';

export default new VueRouter({ 
    base: '',
    routes: [
    { path: '/search/tv', component: SearchComponent, props: {type: "TV"}},
    { path: '/search/movies', component: SearchComponent, props: {type: "Movies"}},
    { path: '/subscriptions/tv', component: SubscriptionsComponent},
    { path: '/future-releases/movies', component: MovieReleasesComponent }
]});