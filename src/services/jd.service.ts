import axios from 'axios';
import authService from './auth.service';

class JdService {
    async addLinks(name: string, linkId: string) {
        const result = await axios.post("http://api.tomfitz.me/jd/add-links", {name,linkId}, {headers: {'Authorization': 'Bearer ' + await authService.token}});
        return result.data;
    }

    async getPackages(): Promise<jdPackage[]> {
        const result = await axios.get('http://api.tomfitz.me/jd/packages', {headers: {'Authorization': 'Bearer ' + await authService.token}});
        return result.data;
    }

    async removeDownload(pkg: jdPackage) {
        const result = await axios.post("http://api.tomfitz.me/jd/remove-package",pkg,{headers: {'Authorization':'Bearer ' + await authService.token }});
        return result.data;
    }
}

export interface jdPackage {
    exVariant: string;
    dlVariant: string;
    bytesLoaded: number;
    name: string;
    running: boolean;
    finished: boolean;
    uuid: number;
    enabled: boolean;
    status: string;
    progressPercent: number;
    speedInMb: number;
    speed: number;
    bytesTotal: number;
    extracting?: boolean;
    extractionProgress?: number; 
    extractionTotal?: number;  
    progress: {
        percent: string;
        eta: string;
        speedInMb: string;
        extraction: string;
    },
    deleteDownloadStatus?: string;
}

const jdService = new JdService();
export default jdService;
