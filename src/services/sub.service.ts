import axios from "axios";
import authService from "./auth.service";

class SubService {

    async getSubscriptions() {
        return await axios.get('http://api.tomfitz.me/subscriptions/tv', {headers: {'Authorization': 'Bearer ' + await authService.token}});
    }

    async addSubscription(name: string, season: number, id: number) {
        return await axios.post('http://api.tomfitz.me/subscriptions/tv', {name, season, id}, {headers: {'Authorization': 'Bearer ' + await authService.token}})

    }
    async deleteSubscription(name: string, season: number, id: number) {
        return await axios.delete('http://api.tomfitz.me/subscriptions/tv', {data: {name,season, id}, headers: {'Authorization': 'Bearer ' + await authService.token}})

    }

    async findTvShow(name: string, id?: number) {
        return await axios.post('http://api.tomfitz.me/tmdb/tv', {name, id}, {headers: {'Authorization': 'Bearer ' + await authService.token}});
    }

    async searchPlexForTvShow(name: string) {
        return await axios.post('http://api.tomfitz.me/plex/get-show', {name}, {headers: {'Authorization': 'Bearer ' + await authService.token}});
    }

    async getMovieSubscriptions() {
        return await axios.get('http://api.tomfitz.me/subscriptions/movie', {headers: {'Authorization': 'Bearer ' + await authService.token}});
    }

    async searchForMovie(name: string, id?: number) {
        return await axios.post('http://api.tomfitz.me/tmdb/movie', {name, id}, {headers: {'Authorization': 'Bearer ' + await authService.token}});
    }

    async searchPlexForMovie(name: string) {
        return await axios.post('http://api.tomfitz.me/plex/get-movie', {name}, {headers: {'Authorization': 'Bearer ' + await authService.token}});
    }

    async addMovieSubscription(name: string, highestQuality: string, id: number) {
        return await axios.post('http://api.tomfitz.me/subscriptions/movie', {name, highestQuality, id}, {headers: {'Authorization': 'Bearer ' + await authService.token}})

    }
}

const subService = new SubService();
export default subService;