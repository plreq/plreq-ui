import axios from 'axios';
import authService from './auth.service';
export class ItiService {
    token: string = "";
    async search(text: string, category: string, quality: string, page: number): Promise<itiLinkResponse> {
        const result = await axios.post('http://api.tomfitz.me/iti/search', {query: text, parent: category, child: quality, page: page}, {headers: {'Authorization': 'Bearer ' + await authService.token}});
        return result.data;
    }
    async searchTv(name: string, season: string, episode: string): Promise<itiLink[]> {
        const result = await axios.post('http://api.tomfitz.me/iti/search/tv', {name, season, episode}, {headers: {'Authorization': 'Bearer ' + await authService.token}});
        return result.data;
    }
    
    async getLinks(linkId: string): Promise<string[]> {
        const result = await axios.get('http://api.tomfitz.me/iti/getLinks/' + linkId, {headers: {'Authorization': 'Bearer ' + await authService.token}});
        return result.data;       
    }

    async tvExists(name: string, season?: number, episode?: number): Promise<boolean> {
        const result = await axios.post('http://api.tomfitz.me/plex/tv-show-exists', {season, episode, name}, {headers: {'Authorization': 'Bearer ' + await authService.token}});
        return result.data;
    }

    async getImageRefs(linkId: string): Promise<string[]> {
        const result = await axios.get('http://api.tomfitz.me/iti/getReferences/' + linkId, {headers: {'Authorization': 'Bearer ' + await authService.token}});
        return result.data;
    }
}

export interface itiLink {
    linkid: string;
    parent: string;
    child: string;
    title: string;
    tags: string;
    datetime: Date;
    links_imgref?: string;
    links: string[];
}

export interface itiLinkResponse {
    page: number;
    results: itiLink[];
}
const itiService = new ItiService();
export default itiService;