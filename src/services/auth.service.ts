import axios from 'axios';
import EventBus, {socket} from '../event-bus';
import * as jwtDecode from 'jwt-decode';
class AuthService {
    token: string; 
    user: any;
    constructor() {
        this.token = localStorage.token;
        if (this.isAuthorized) {
            socket.on('connect', () => 
                socket.emit('authorization', this.token)
            );
            const decoded = jwtDecode(this.token);
            this.user = {};
            this.user.userGuid = decoded.userGuid;
            this.user.level = decoded.level;
            this.user.username = decoded.username;
            EventBus.$emit('hasToken', this.user);
        }
        EventBus.$emit('hasToken', false);
    }

    get isAuthorized(): boolean {
        if (this.token) {
            const decoded = jwtDecode(this.token);
            return Date.now() < decoded.exp*1000;
        }
        return false;
    }

    async getToken(user: string, pass: string, decode: boolean = false) : Promise<string|any> {
        if (this.isAuthorized) {
            EventBus.$emit('hasToken', true);
            if (decode) {
            return jwtDecode(this.token);
            }
            return this.token;
        }
        const result = await axios.post('http://api.tomfitz.me/auth/token', {username: user, password: pass });
        this.token = result.data;
        if (this.isAuthorized) {
            var decoded = jwtDecode(result.data);
            this.user = this.user || {};
            this.user.userGuid = decoded.userGuid;
            this.user.level = decoded.level;
            this.user.username = decoded.username;
            localStorage.token = this.token;
            EventBus.$emit('hasToken', true);
            socket.emit('authorization', this.token);
            if (decode) {
                return jwtDecode(this.token);
            }
        }
        return this.token;
    }

    removeToken() {
        this.token = "";
        localStorage.removeItem('token');
        EventBus.$emit('hasToken', false);
        socket.emit('logout', true);
    }

    async createUser(userGuid: string, username: string, password: string) {
        return new Promise<boolean>((resolve) => 
            socket.emit('createUser', {userGuid,username,password}, resolve)
        );
    }

}



const authService = new AuthService;
export default authService;