import Vue from 'vue';
import VueRouter from 'vue-router';
import SearchComponent from './components/Search.vue';
import LoginComponent from './components/Login.vue';
import BodyComponent from './components/Body.vue';
import VideoComponent from './components/Video.vue';
import StatusBar from './components/StatusBar.vue';
import AlertsComponent from './components/Alerts.vue';
import SubscriptionsComponent from './components/Subscriptions.vue';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import './style.css';
import headroom from 'vue-headroom';
import router from './router';
Vue.use(BootstrapVue);
Vue.use(headroom);
Vue.use(VueRouter);
let v = new Vue({
    el: "#app",
    template: `
        <body-component></body-component>
    `,
    router,
    components: {
        StatusBar,
        LoginComponent,
        SearchComponent,
        SubscriptionsComponent,
        BodyComponent,
        VideoComponent,
        AlertsComponent
    }
});
